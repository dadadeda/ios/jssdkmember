//
//  JSSDKMember.m
//  JSSDKMember
//
//  Created by 胡学礼 on 2020/10/10.
//

#import "JSSDKMember.h"

@implementation JSSDKMemberConfig


@end

@interface JSSDKMember()
@property (nonatomic, strong) JSSDKMemberConfig* config;
@end


@implementation JSSDKMember

+ (instancetype)share{
    static dispatch_once_t onceToken;
    static JSSDKMember* _instance;
    dispatch_once(&onceToken, ^{
        _instance = [JSSDKMember new];
    });
    return _instance;
}
- (void)setConfig:(JSSDKMemberConfig*)config{
    _config = config;
}

- (void)output{
    NSLog(@"JSSDK---output");
}
@end
