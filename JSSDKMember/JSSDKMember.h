//
//  JSSDKMember.h
//  JSSDKMember
//
//  Created by 胡学礼 on 2020/10/10.
//

#import <Foundation/Foundation.h>

@interface JSSDKMemberConfig : NSObject
@property (nonatomic, copy) NSString* _Nonnull loginApi;
@property (nonatomic, copy) NSString* _Nonnull registerApi;
@property (nonatomic, copy) NSString* _Nonnull smsApi;
@end

NS_ASSUME_NONNULL_BEGIN

@interface JSSDKMember : NSObject
+ (instancetype)share;
- (void)setConfig:(JSSDKMemberConfig*)config;
- (void)output;
@end

NS_ASSUME_NONNULL_END
