//
//  main.m
//  JSSDKMember
//
//  Created by Carrot on 10/10/2020.
//  Copyright (c) 2020 Carrot. All rights reserved.
//

@import UIKit;
#import "SDKJSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDKJSAppDelegate class]));
    }
}
