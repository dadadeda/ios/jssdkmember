//
//  SDKJSAppDelegate.h
//  JSSDKMember
//
//  Created by Carrot on 10/10/2020.
//  Copyright (c) 2020 Carrot. All rights reserved.
//

@import UIKit;

@interface SDKJSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
