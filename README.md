# JSSDKMember

[![CI Status](https://img.shields.io/travis/Carrot/JSSDKMember.svg?style=flat)](https://travis-ci.org/Carrot/JSSDKMember)
[![Version](https://img.shields.io/cocoapods/v/JSSDKMember.svg?style=flat)](https://cocoapods.org/pods/JSSDKMember)
[![License](https://img.shields.io/cocoapods/l/JSSDKMember.svg?style=flat)](https://cocoapods.org/pods/JSSDKMember)
[![Platform](https://img.shields.io/cocoapods/p/JSSDKMember.svg?style=flat)](https://cocoapods.org/pods/JSSDKMember)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JSSDKMember is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JSSDKMember'
```

## Author

Carrot, xueli.hu@get88.cn

## License

JSSDKMember is available under the MIT license. See the LICENSE file for more info.
